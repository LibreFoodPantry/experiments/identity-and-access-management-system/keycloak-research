# Keycloak Introduction
Keycloak is a Java based Authentication & Authorization Server
Features:
- Single Sign-on and Signle Sign-out
- Standard Protocols: OAuth2.0, OIDC 1.0., SAML 2.0
- Flexible Authentication and Authorization
- Mutli-factor Authentication: One- Time Passwords
- Social Login: Google, Facebook, Twitter,
- Provides centralized User management.
- Supports Directory Services.
- Customizable and Extensible.
- Easy setup and Integration.


# Keycloak Administration Guide:
 Keycloaks Documentation referring to key basic concepts. 
 https://www.keycloak.org/docs/latest/server_admin/#keycloak-features-and-concepts

# What is a Realm?
![Image of realms](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjEonL1nUj_Yj8mJ00cPZc6WALjl5IULI6-Q&usqp=CAU)
Configuring Realm Settings:
 https://www.keycloak.org/docs/latest/server_admin/#configuring-realms

# Keycloak Technology Stack
![Image of TechStack](https://files.speakerdeck.com/presentations/80bb4d0f851f4959b4ca1472041b5c89/slide_10.jpg)

# Sources:
- Link1: "Keycloak Basics Video"
- https://www.youtube.com/watch?v=FyVHNJNriUQ
- Link2: "Keycloak Server Administration Guide"
- https://www.keycloak.org/docs/latest/server_admin/#keycloak-features-and-concepts
- Link3: "Configuring Realm Settings"
- https://www.keycloak.org/docs/latest/server_admin/#configuring-realms
