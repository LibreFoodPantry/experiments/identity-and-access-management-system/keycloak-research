## KEYCLOAK GUIDE

**Here is an instructional guide for using Keycloak with your computer/software.**

## SET-UP INSTRUCTIONS

## STEP 1
- Clone the "DockerWithoutKeycloak" repository to your computer. (Best done using HTTPS).

![Error: Image not found.](/)

## STEP 2
- Extract the files from each of the two folders, "keycloak-quickstarts" and "keycloak-17.0.0".
<br>

**Note:** Due to the amount of file within these folders, the process can take up to 90 minutes.

![Error: Image not found.](/)

## STEP 3
- Once the files have been extracted, you may need to download Node.js.
- You may also need to download a JDK (Java Development Kit).

![Error: Image not found.](/)

## STEP 4
- Open up a Powershell terminal (on Windows).
- Go to "./keycloak-quickstarts/keycloak-quickstarts/applications/app-vue.
- When at this location, run the command "npm run dev"

![Error: Image not found.](/)

- Open up a second Powershell terminal.
- Go to "./keycloak-17.0.0/keycloak-17.0.0.
- When at this location, run the command "bin/kc.bat start-dev".
<br>

**Note:** If done correctly, your powershell terminal should look like the image below.
<br>

![Error: Image not found.](/)

**Note:** for this step, we are unsure of how to run an equivalent process for Mac computers.

## EXECUTION INSTRUCTIONS.

Once the kit has been set-up, follow these instructions to execute the app.

## STEP 1

![Error: Image not found.](/guide_index.png)

- Start at the "index.html" file.
- Within this file, there will be a "Click Here to Login" button. This button will show an alert saying "redirecting to Keycloak..."
- After closing the alert, you should be sent to localhost:8080 (the Keycloak re-direct).

![Error: Image not found.](/guide_redirect.png)
## STEP 2

![Error: Image not found.](/guide_keycloak.png)

- At this point, you should be expected to enter a username and password, as shown below.
<br>

**NOTE:** This step may require going to the "master realm" of Keycloak beforehand. In the master realm, you will set up users/credentials that will be used for the login.

## STEP 3

![Error: Image not found.](/guide_vue.png)

- Upon entering successful credentials, you will be inside the VUE app.

You have completed the process of setting up and executing our Keycloak demo. Congratulations!