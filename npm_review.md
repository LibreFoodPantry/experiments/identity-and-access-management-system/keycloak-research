NPM is a package manager that comes bundled with node.js. It is used to manage packages and dependencies within your front end application. It is extremely powerful because it can take into account dependencies within a package.json and automatically install every necessary one with the npm install command. While it is not entirely necessary, it takes a lot of the busy work out of managing these dependencies manually.

**Functionality:**
-	Download tools
-	Manage packages
-	Share code with other npm users
-	Manage code dependencies

**Read more here:** About npm | npm Docs (npmjs.com) 
