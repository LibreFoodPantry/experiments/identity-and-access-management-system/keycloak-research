# Istio Ingress Gateway Research

## API Access Control using Istio Ingress Gateway
## Istio Ingress Gateway:
Istio is widely used as a Service Mesh solution for Cloud-native applications. It offers a plethora of features across Security, Traffic Management and Observability. The Istio Ingress Gateway component caters to managing traffic entering into the service mesh.
Istio Ingress Gateway can be used as a API-Gateway to securely expose the APIs of your micro services. It can be easily configured to provide access control for the APIs i.e. allowing you to apply policies defining who can access the APIs, what operations they are allowed to perform and much more conditions. I’ll explain how to implement API access control using Istio Ingress Gateway.
## JWT Access Token:
The first step in implementing access control is to establish the identity of the user and encode his/her access privileges in an Access Token. This is a very common pattern/approach in securing web applications. The User (or the browser/client acting on behalf of the user) should first authenticate with an Identity provider (IdP). After successfully authenticating the user, the IdP issues an Access token.
Istio supports Access tokens of JWT format. JWT(JSON Web Token) is an open, industry standard (RFC 7519) method for representing claims securely between two parties.
## Target Environment:
![Image of Diagram1](https://miro.medium.com/max/1400/1*9AkOTg0ut666EHa_w85QZg.jpeg)
## API Access Conditions:
The goal is to:- 
    - Expose the application at endpoint: http://example.com/app/nginx
    - Users must first authenticate themselves with the Identity Provider application and receive the JWT Access token.
    -The User is allowed to access ONLY the /nginx path.
    -The Client MUST present a valid and fresh JWT Access token in the “Authorization” HTTP header.
    - The Token must have the “iss” and “sub” claims equal to “testing@secure.istio.io”
## Ingress Manifest:
- The Ingress below will proxy the URL www.example.com/app/* to the Istio Ingress gateway running in “istio-system” namespace. Note the “path” field in the manifest and also the “rewrite-target” annotation. Both these configurations work together to remove the “app” path in the request. So when the request reaches the Istio Ingress gateway it would be: www.example.com/nginx.

```sh
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/rewrite-target: /$2
  name: istio-ingressgateway
  namespace: istio-system
spec:
  rules:
  - host: www.example.com
    http:
      paths:
      - backend:
          serviceName: istio-ingressgateway
          servicePort: 80
        path: /app(/|$)(.*)
```
## Istio Gateway Manifest:
- The Gateway configuration below will be applied to the Istio Ingress gateway running in “istio-system” namespace. The labels in the “selector” field will pick out the Istio Ingress gateway Pod. This configuration will open up the Gateway for the host “www.example.com” and the port “80” for “http” protocol. Any request not fulfilling this configuration will be rejected.
```sh
apiVersion: networking.istio.io/v1beta1
kind: Gateway
metadata:
  name: my-gateway
  namespace: istio-system
spec:
  selector:
    app: istio-ingressgateway
    istio: ingressgateway
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - www.example.com
```
## Istio RequestAuthentication Manifest:
- The RequestAuthentication manifest below is also applied on the Istio Ingress Gateway. It instructs the gateway to apply the configured JWT rules when the request contains a JWT Access token in the “Authorization” HTTP header. It expects the issuer of the token i.e. claim field “iss”, to be equal to “testing@secure.istio.io”. It uses the JWKS (JSON Web Key Set) fetched from the “jwksUri” to validate the signature of the presented JWT token. When a IdP issues a token, it signs it using it’s private key. The Gateway below validates the signature of the token using the JWKS (Public key) of the IdP. This ensures the token issued by the IdP is not tampered in any manner. Any tampered tokens will fail the JWT rules and the request rejected.
We’d also need to reject any requests that do not have an Access token. This is achieved by creating a “VirtualService” and linking it to the RequestAuthentication below.
```sh
aapiVersion: security.istio.io/v1beta1
kind: RequestAuthentication
metadata:
  name: my-request-authentication
  namespace: istio-system
spec:
  selector:
    matchLabels:
      app: istio-ingressgateway
      istio: ingressgateway
  jwtRules:
  - issuer: "testing@secure.istio.io"
    jwksUri: "https://raw.githubusercontent.com/istio/istio/release-1.7/security/tools/jwt/samples/jwks.json"
```
## Istio AuthorizationPolicy Manifest:
The Authorization Policy below will be applied by the Istio Ingress Gateway BEFORE it decides to forward the request to the actual application pod. The rules in the below configuration would:-
- Only allow requests from principal “testing@secure.istio.io/testing@secure.istio.io”. Istio computes the request principal field by joining the “iss” and “sub” claims in the Token, with a “/” in between.
- Only allow requests to “/nginx” path
- Any other requests are rejected.
```sh
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: my-authorization-policy
  namespace: istio-system
spec:
  selector:
    matchLabels:
      app: istio-ingressgateway
      istio: ingressgateway
  action: ALLOW
  rules:
  - from:
    - source:
        requestPrincipals: ["testing@secure.istio.io/testing@secure.istio.io"]
    to:
    - operation:
        paths: ["/nginx"]
```
## Istio VirtualService Manifest:
The VirtualService configuration below defines a Istio virtual service. This virtual service is linked to the Gateway configuration we defined earlier. The below configuration will allow only requests containing the “/nginx” path after the host name “www.example.com”. It’s also configured to rewrite the URL so that the “/nginx” path is removed when Istio Ingress Gateway forwards the request to the “destination” defined in the virtual service.
The destination mentioned in the below configuration points to the name of the Kubernetes Service object and the port configured for the service.
```sh
apiVersion: networking.istio.io/v1beta1
kind: VirtualService
metadata:
  name: my-virtualservice
  namespace: app
spec:
  hosts:
  - www.example.com
  gateways:
  - istio-system/my-gateway
  http:
  - match:
    - uri:
        prefix: /nginx
    rewrite:
      uri: /
    route:
    - destination:
        host: nginx
        port:
          number: 80
    name: "nginx"
```
#
## - How to implement a User Authentication solution using Istio Ingress Gateway, OAuth2-Proxy and Keycloak. 

- Solutions goals are to check if the user requesting access to an Application/API is already authenticated or not, and if not already authenticated, authenticate the user and have the JWT access token generated and attached to the request. 
## Overview of the components.
## - Istio Ingress Gateway
- Istio Ingress Gateway runs in the edge of the service mesh, managing traffic entering into the mesh. However it can be deployed as a stand-alone service (along with Istiod) as well, without having a mesh. It is widely used to implement a centralized API Gateway function to protect the APIs exposed by multiple micro-services running behind the Gateway.
-  When a user wants to access an Application from a browser, the user needs to be authenticated first (for e.g. by username and password). Istio Ingress Gateway can only authenticate an incoming request based on the JWT access token attached to the request. It cannot authenticate a user on its own, for e.g. by presenting a login form or via HTTP basic authentication. For authenticating a user, the authentication process has to be delegated to an external Identification provider like Keycloak.
## - Keycloak
- Keycloak is a prominent open source Identity and Access Management (IAM) solution. It is capable of authenticating a user via OpenID Connect protocol. This protocol is based on OAuth2.0 standard, which prescribes several mechanisms (called grant types) for authenticating users. The use of OAuth2.0 helps in delegating the authentication to an external system, thereby relieving the application from having to do so. This is also very secure since the user needs to share his/her credentials only with the Identity Provider (Keycloak).
In the solution that I’ll be explaining, we’ll use Keycloak as the Identity Provider i.e. the user has an account in the Keycloak and it is responsible for authenticating the user by validating the username and password. In short, it performs the role of OAuth Server.
## - OAuth2-Proxy
 - OAuth2-Proxy is an open source reverse-proxy solution that performs the role of OAuth Client in a OAuth2.0 authentication flow. It is capable of detecting if the incoming request is already authenticated or not, and can orchestrate the authentication of the user with the OAuth Server (Keycloak). It receives the access token from Keycloak and stores it in its backend storage. It automatically refreshes the access token, by using refresh tokens. In our solution, we’ll make use of OAuth2-Proxy to co-ordinate the authentication flow between Istio Ingress Gateway and Keycloak
## How the solution works. 
![Image of Diagram2](https://miro.medium.com/max/1400/1*imAR3M6xgLlq78Fk78Nakw.png)

## Sources:
- Link1: "API Access Control using Istio Ingress Gateway"
- https://medium.com/@senthilrch/api-access-control-using-istio-ingress-gateway-44be659a087e
- Link2: "API Authentication using Istio Ingress Gateway, OAuth2-Proxy and Keycloak"
- https://medium.com/@senthilrch/api-authentication-using-istio-ingress-gateway-oauth2-proxy-and-keycloak-a980c996c259
